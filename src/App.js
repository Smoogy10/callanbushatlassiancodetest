import React, { Component } from 'react';
import queryString from 'query-string';
import { capitalize } from 'lodash/fp';
import Header from './components/Header/Header';
import SubHeader from './components/SubHeader/SubHeader';
import VideoArchive from './components/VideoArchive/VideoArchive';
import './App.css';
import * as data from './data/sessions.json';

class App extends Component {
  constructor(props) {
    super(props);

    const urlQuery = queryString.parse(window.location.search);
    this.state = {
      data: data,
      track: capitalize(urlQuery.track) || null,
      session: capitalize(urlQuery.session) || null,
    };
  }

  render() {
    return (
      <div className="App">
        <Header />
        <SubHeader />
        <VideoArchive data={this.state.data} track={this.state.track} session={this.state.session} />
      </div>
    );
  }
}

export default App;
