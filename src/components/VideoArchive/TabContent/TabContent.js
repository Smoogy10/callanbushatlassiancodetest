import React, { Component } from 'react';
import { Col, Nav, NavItem, Row, Tab } from 'react-bootstrap';
import { map } from 'lodash/fp';
import queryString from 'query-string';
import './TabContent.css';

class TabContent extends Component {
  setUrl = (obj) => {
    window.location.search = queryString.stringify(obj);
  };

  render() {
    const { content } = this.props.content;
    const tabs = map(item => { 
      return { 
        id: item.Id,
        title: item.Title, 
        speaker: { 
          firstName: item.Speakers ? item.Speakers[0].FirstName : '',
          lastName: item.Speakers ? item.Speakers[0].LastName : '',
          company: item.Speakers ? item.Speakers[0].Company : '',
        }
      }}
      )(content);
    const tabContent = map(item => {
      return {
        id: item.Id,
        title: item.Title,
        speaker: { 
          firstName: item.Speakers ? item.Speakers[0].FirstName : null,
          lastName: item.Speakers ? item.Speakers[0].LastName : null,
          company: item.Speakers ? item.Speakers[0].Company : null,
          biography: item.Speakers ? item.Speakers[0].Biography : null,
        },
        description: item.Description,
      }
    })(content);

    return (
      <Tab.Container defaultActiveKey={this.props.session || tabs[0].id} className="tabContent">
        <Row className="clearfix">
          <Col sm={4}>
            <div className="trackHeader">
              {this.props.content.track}
            </div>
            <Nav bsStyle="pills" stacked>
            {
              map(item => {
                const speakerInfo = item.speaker.firstName && item.speaker.lastName ? <p>{item.speaker.firstName} {item.speaker.lastName}, {item.speaker.company}</p> : null;
                return(
                  <NavItem className="tabContainer" eventKey={item.id} onClick={() => { this.setUrl({ track: this.props.content.track, session: item.id }) }}>
                    <div className="tabHeader">
                      {item.title}
                    </div>
                    <div className="tabSubHeader">
                      {speakerInfo || null}
                    </div>
                  </NavItem>
                );
              })(tabs)
            }
            </Nav>
          </Col>
          <Col sm={8}>
            <Tab.Content animation>
            {
              map(item => {
                const speakerInfo = item.speaker.firstName && item.speaker.lastName ? <p className="speakerInfo">{item.speaker.firstName} {item.speaker.lastName}, {item.speaker.company}</p> : null;
                return (
                  <Tab.Pane eventKey={item.id} className="tabPane">
                    <h1>{item.title}</h1>
                    {speakerInfo}
                    <p className="description">{item.description}</p>
                    <a className="qandaLink">{speakerInfo ? 'See the Q&A from this talk and others here.' : null}</a>
                    <h2>{speakerInfo ? 'About the speaker' : null}</h2>
                    {speakerInfo}
                    <p>{speakerInfo ? item.speaker.biography : null}</p>
                  </Tab.Pane>
                )
              })(tabContent)
            }
            </Tab.Content>
          </Col>
        </Row>
      </Tab.Container>
    );
  }
}

export default TabContent;