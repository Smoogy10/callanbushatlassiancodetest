import React, { Component } from 'react';
import TabContent from './TabContent/TabContent';
import { Tab, Tabs } from 'react-bootstrap';
import { find, flow, groupBy, map, uniqBy, value } from 'lodash/fp';
import queryString from 'query-string';
import './VideoArchive.css';

class VideoArchive extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tabs: [],
      contentGroups: {},
    };
  }

  componentDidMount() {
    const items = this.props.data.Items;
    const tabs = map((item) => {
      return item.Track.Title;
    })(uniqBy('Track.Title')(items));

    const contentGroups = flow(
      groupBy(x => x.Track.Title),
      map.convert({cap: false})((value, key) => ({ track: key, content: value }))
    )(items);

    this.setState({ contentGroups, tabs });
  }

  setUrl = (obj) => {
    window.location.search = queryString.stringify({ track: obj });
  }

  render() {
    const { contentGroups, tabs } = this.state;

    return(
      <div className="videoArchive">
        <Tabs defaultActiveKey={this.props.track || tabs[0]} onSelect={this.setUrl}>
        {
          map(item => {
            return (
                <Tab eventKey={item} title={item} className="tab">
                  <TabContent session={this.props.session} setUrl={this.setUrl} content={find((value) => value.track === item)(contentGroups)}/>
                </Tab>
            );
          })(tabs)
        }
        </Tabs>
    </div>
    );
  }
}

export default VideoArchive;