import React, { Component } from 'react';
import { AtlassianLogo } from '@atlaskit/logo';
import './Header.css';

class Header extends Component {
  render() {
    return(
      <header className="header">
        <div className="headerContainer">
          <AtlassianLogo textColor="white" iconColor="white" className="logo"/>
        </div>
      </header>
    );
  }
}

export default Header;