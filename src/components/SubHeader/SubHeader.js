import React, { Component } from 'react';
import { AtlassianLogo } from '@atlaskit/logo';
import {
  akColorB400,
  akcolorN90,
  akColorN800
} from '@atlaskit/util-shared-styles';

import './SubHeader.css';

class SubHeader extends Component {
  render() {
    return(
      <div className="subheader">
        <div className="content">
          <AtlassianLogo iconColor={akColorB400} textColor={akColorB400} size="large" className="logo"/>
          <span style={{"color": akColorN800}}>Summit</span>
          <div style={{"color": akcolorN90}}>
            Video Archive
          </div>
        </div>
      </div>
    );
  }
}

export default SubHeader;