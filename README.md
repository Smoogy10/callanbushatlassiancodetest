# Callan Bush - Atlassian Code Test

## Install
```
npm install
yarn install
npm start
```

## Notes

First off, I'd like to thank you for giving me this incredible opportunity to interview at a company I love. As a true blue Aussie, it's so inspiring to see Atlassian kill it out in the rough waters that is Silicon Valley. You're all doing an amazing job. Also, fun fact: I used to live near your Sydney HQ!

Here are some thoughts:

* All in all, the task took me about 4 hours. 3 hours for the core functionality, and 1 hour for cleaning things up + adding the URL session feature.
* It looks like I've covered all of the basics and bonus points! The app is responsive to the extent that it doesn't break when resized, but ideally I would have liked to take it a bit further, like modularising the horizontal tabs into a hamburger menu.
* Very sneaky about the General and Activities tabs! I hid the speaker-related stuff for those.
* If given more time, I definitely would have included unit tests. Jest and Enzyme would have been my tools of choice.